package com.wf.wxsign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WxSignApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxSignApplication.class, args);
    }

}

